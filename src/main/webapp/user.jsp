<%--
  Created by IntelliJ IDEA.
  User: 008
  Date: 2022/7/7
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户管理</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function () {
            $('#dg').datagrid({
                pagination:true,
                toolbar:'#tb',
                fit:true,
                fitColumns:true,
                url:'/user/listUser',
                columns:[[
                    {field:'uid',checkbox:true},
                    {field:'uname',title:'用户名',width:100},
                    {field:'nickname',title:'用户昵称',width:100},
                    {field:'password',title:'用户密码',width:100},
                    {field:'sex',title:'性别',width:100},
                    {field:'avatar',title:'头像',width:100},
                    {field:'remark',title:'备注',width:100},
                    {field:'ctime',title:'创建时间',width:100},
                    {field:'mtime',title:'修改时间',width:100}
                ]]
            });
        })

        function showUpdateUser() {
            const arr = $('#dg').datagrid('getSelections');
            if (arr.length != 1) {
                $.messager.alert('错误', '请选择一个', 'error');
                $('#dg').datagrid('unselectAll');
            } else {
                $('#update-form').form('load', {
                    uid:arr[0].uid,
                    uname:arr[0].uname,
                    nickname: arr[0].nickname,
                    password: arr[0].password,
                    sex: arr[0].sex,
                    avatar: arr[0].avatar,
                    remark: arr[0].remark,
                })
                $("#update-diglog").dialog({
                    closed: false,
                    buttons: [{
                        text: '修改用户信息',
                        iconCls: 'icon-edit',
                        handler: function () {
                            $('#update-form').form('submit', {
                                url: '/user/updateUser',
                                onSubmit: function () {
                                    return $('#update-form').form('validate');
                                },
                                success: function (data) {
                                    if (data!=0) {
                                        $("#update-diglog").dialog({
                                            closed: true
                                        })
                                        $("#dg").datagrid('reload')
                                        $('#update-form').form('clear')
                                        $.messager.show({
                                            title: '温馨提示',
                                            msg: '修改成功',
                                            timeout: 3000,
                                            showType: 'slide'
                                        });
                                    } else {
                                        $.messager.alert('错误', '只能修改自己账户', 'error');
                                    }
                                }
                            });
                        }
                    }, {
                        text: '重置数据',
                        iconCls: 'icon-redo',
                        handler: function () {
                            $('#update-form').form('load', {
                                uid:arr[0].uid,
                                uname:arr[0].uname,
                                nickname: arr[0].nickname,
                                password: arr[0].password,
                                sex: arr[0].sex,
                                avatar: arr[0].avatar,
                                remark: arr[0].remark,
                            })
                        }
                    }]
                })
            }
        }

        function removeUser() {
            const arr = $('#dg').datagrid('getSelections');
            if (arr.length == 0) {
                $.messager.alert('错误', '至少选择一个数据', 'error');
            } else {
                var flag = confirm("是否确认删除");
                if (flag) {
                    var ids = [];
                    for (const a in arr) {
                        ids.push(arr[0].uid);
                    }
                    $.post('/user/removeUser', {'ids[]': ids}, function (flag) {
                        if (flag) {
                            $.messager.show({
                                title: '温馨提示',
                                msg: '删除成功',
                                timeout: 3000,
                                showType: 'slide'
                            });
                            $("#dg").datagrid('reload')
                        } else {
                            $.messager.alert('错误', '只能删除自己账户', 'error');
                        }
                    })
                }
            }
        }

        function searchUname() {
            let uname = $('#searchUname').val();
            $('#dg').datagrid('reload', {
                uname: uname
            })
        }
    </script>
</head>
<body>
<table id="dg"></table>
<div id="tb">
    <input id="searchUname" type="text" placeholder="请输入要搜索的内容">
    <a onclick="searchUname()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
    <a onclick="showUpdateUser()" href="#" class="easyui-linkbutton">修改用户信息</a>
    <a onclick="removeUser()" href="#" class="easyui-linkbutton">删除用户</a>
</div>
<div id="update-diglog" closed="true" class="easyui-dialog" title="修改用户信息" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="update-form">
        <p style="display: inline;margin-right: 10px">用户昵称:</p>
        <input name="nickname" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">用户密码:</p>
        <input name="password" class="easyui-passwordbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">确认密码:</p>
        <input class="easyui-passwordbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">性别:</p>
        <input name="sex" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">头像:</p>
        <input name="avatar" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">备注:</p>
        <input name="remark" class="easyui-textbox" required="required" style="width:300px">
        <input type="hidden" name="uname">
        <input type="hidden" name="uid">
    </form>
</div>
</body>
</html>
