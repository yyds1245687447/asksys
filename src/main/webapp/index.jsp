<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<html>
<head>
    <title>问答管理系统</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        function openTabs(url,text) {
            if ($("#tt").tabs('exists', text)) {
                $("#tt").tabs('select', text);
            } else {
                let myContext = "<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src=" + url + "></iframe>";
                $("#tt").tabs('add', {
                    title: text,
                    closable: true,
                    content: myContext
                })
            }
        }
    </script>
</head>
<body>
<div id="cc" class="easyui-layout" style="width:99.5%;height:930px;">
    <div data-options="region:'north',title:'问答管理系统',split:true" style="height:80px;background-color: rgba(244,244,251,0.45)">
        <span style="font-size: 25px;color: #0c7cd5;line-height:43px;position: relative;left: 150px">
            欢迎：
            <shiro:principal/>
             登录
            <a href="/zhuxiao" style="margin-right:440px;float:right;text-decoration:none">注销</a>
        </span>
    </div>
    <div data-options="region:'west',title:'导航',split:true" style="width:298px;">
        <div id="aa" class="easyui-accordion" style="width:300px;height:100%;">
            <div title="提问管理">
                <a onclick="openTabs('tagList.jsp','问题分类')" href="#" class="easyui-linkbutton" style="width: 100%">问题分类管理</a>
                <a onclick="openTabs('question.jsp','问题管理')" href="#" class="easyui-linkbutton" style="width: 100%">问题管理</a>
                <a onclick="openTabs('commentList.jsp','评论管理')" href="#" class="easyui-linkbutton" style="width: 100%">评论管理</a>
                <a onclick="openTabs('answer.jsp','回复管理')" href="#" class="easyui-linkbutton" style="width: 100%">回复管理</a>
                <a onclick="openTabs('user.jsp','用户管理')" href="#" class="easyui-linkbutton" style="width: 100%">用户管理</a>
                <a onclick="openTabs('permissionslist.jsp','权限管理')" href="#" class="easyui-linkbutton" style="width: 100%">权限管理</a>
            </div>
        </div>
    </div>
    <div data-options="region:'center',title:'内容展示'" style="padding:5px;">
        <div id="tt" class="easyui-tabs" fit="true">
            <div title="首页" style="padding:20px;display:none;">
                <div title="" style="padding: 0; width: 1370px; height: 771px;" class="panel-body panel-body-noheader panel-body-noborder">
                    <div title="" style="padding: 0; width: 1370px; height: 771px;background-image: url('1.jpg')" class="panel-body panel-body-noheader panel-body-noborder" >
                        <h1 style="text-align: center;font-size: 50px;color: #6ae9f7">欢迎使用问答管理系统</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

