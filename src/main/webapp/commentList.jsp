<%--
  Created by IntelliJ IDEA.
  User: 爱大米
  Date: 2022/7/7
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>评价</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function (){
            $('#dp').datagrid({
                pagination:true,
                fit:true,
                toolbar:'#ta',
                fitColumns:true,
                url:'/question/listQuestion',//controller路径
                //field:对应数据库列名，title:表格标题
                columns:[[
                    {field:'qid',title:'qid',width: 50},
                    {field:'uname',title:'用户名',width:50},
                    {field:'tname',title:'分类名',width:50},
                    {field:'title',title:'标题',width:50},
                    {field:'content',title:'内容',width:50},
                    {field:'ctime',title:'创建时间',width:50},
                    {field:'mtime',title:'修改时间',width:50},
                    //value表示属性值用不上，row表示当前行数据，index表示行下标
                    {field:'xxx',title:'评价问题',width:100,formatter:function (value,row,index){
                            return "<input onclick='commentQuestion("+row.qid+")' type='button' value='评价问题'/>";
                        }}
                ]]
            });
        })
        function commentQuestion(qid){
            $('#comment-form').form('load', {
                qid:qid
            })

            //alert("qid"+qid);
            $("#comment-dialog").dialog({
                closed:false,
                buttons:[{
                    text:'评论问题',
                    iconCls:'icon-save',
                    handler:function(){
                        $('#comment-form').form('submit',{
                            url:"/comment/saveComment",
                            onSubmit: function(){
                                return $('#comment-form').form('validate');
                            },
                            //提交成功以后的操作，data为返回值类型
                            success:function(data){
                                if(data){
                                    $("#comment-dialog").dialog({
                                        closed:true,
                                    })
                                    $('#dg').datagrid('reload')
                                    $('#comment-form').form('clear')
                                }else{
                                    alert("提交失败");
                                }

                            }
                        });
                    }
                },{
                    text:'重置数据',
                    iconCls:'icon-redo',
                    handler:function(){
                        $('#comment-form').form('clear')
                    }
                }]
            })
        }
        function searchQuestion(){
            var title = $("#searchQuestion").val();
            $("#dp").datagrid('reload',{
                title:title
            })
        }
    </script>
    <script>
        $(function (){
            $('#dg').datagrid({
                pagination:true,
                fit:true,
                toolbar:'#tb',
                fitColumns:true,
                url:'/comment/listComment',//controller路径
                //field:对应数据库列名，title:表格标题
                columns:[[
                    {field:'cid',checkbox:true},
                    {field:'uname',title:'用户名',width:100},
                    {field:'content',title:'问题内容',width:100},
                    {field:'cmcontent',title:'评论内容',width:100},
                    {field:'ctime',title:'创建时间',width:100},
                    {field:'mtime',title:'修改时间',width:100,align:'right'}
                ]]
            });
        })
    </script>
    <script>
        function searchComment(){
            var cmcontent = $("#searchComment").val();
            $("#dg").datagrid('reload',{
                cmcontent:cmcontent
            })
        }
    </script>
    <script>
        function showUpdateDialog(){
            var arr=$('#dg').datagrid('getSelections');
            if(arr.length!=1){
                //alert("请选择一行");
                $.messager.alert('警告','请选择一行','warning');
                $('#dg').datagrid('unselectAll')
            }else {

                $('#update-form').form('load',{
                    cid:arr[0].cid,
                    cmcontent:arr[0].cmcontent,
                    tid:arr[0].tid
                })

                $("#update-dialog").dialog({
                    closed:false,
                    buttons:[{
                        text:'修改评价',
                        iconCls:'icon-edit',
                        handler:function(){
                            $('#update-form').form('submit',{
                                url:"/comment/updateComment",
                                onSubmit: function(){
                                    return $('update-form').form('validate');
                                },
                                //提交成功以后的操作，data为返回值类型
                                success:function(data){
                                    if(data==true){
                                        $("#update-dialog").dialog({
                                            closed:true,
                                        })
                                        $('#dg').datagrid('reload')
                                        $('#update-dialog').form('clear')
                                        $.messager.show({
                                            title:'温馨提示',
                                            msg:'提交成功',
                                            timeout:1000,
                                            showType:'slide'
                                        });
                                    }else{
                                        alert("提交失败");
                                    }

                                }
                            });
                        }
                    },{
                        text:'重置数据',
                        iconCls:'icon-redo',
                        handler:function(){
                            $('#update-form').form('load',{
                                cid:arr[0].cid,
                                cmcontent:arr[0].cmcontent,
                                tid:arr[0].tid
                            })
                        }
                    }]

                })
            }
        }
    </script>
    </script>
    <script>
        function removeComment(){
            //获取用户选择的数据，并保存到数组中
            var arr=$('#dg').datagrid('getSelections');
            //遍历数组 通过a.属性的方法获取具体的属性值
            //  for(var a of arr){
            //      alert(a.content);
            //  }
            if(arr.length==0){
                alert("请至少选择一行数据")
            }else {
                var flag =confirm("是否确认删除");
                //  alert("flag"+flag)
                if(flag==true){
                    //执行删除操作
                    //创建一个空数组
                    var ids=[];
                    for(var a of arr){
                        ids.push(a.cid);
                    }
                     //alert(ids);
                    //向后台发送数据
                    //第一个参数返回路径，第二个向后台发送的数据，第三个是回调函数
                    $.post('/comment/removeComment',{'ids[]':ids},function (flag){
                        if(flag){
                            //刷新一下
                            $('#dg').datagrid('reload')
                        }else
                        {
                            alert("删除失败");
                        }

                    });
                }
            }
        }
    </script>
</head>
<body>

    <div style="height: 50% ">
        <table id="dp"></table>
        <div id="ta">
            <input id="searchQuestion" type="text" placeholder="请输入搜索的问题标题">
            <a onclick="searchQuestion()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
        </div>

        <div id="comment-dialog" closed="true" class="easyui-dialog" title="My Dialog" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
            <form id="comment-form">
                问题内容：<textarea name="cmcontent" style="width: 100%;height: 100%" ></textarea>
                <input type="hidden" name="qid">
                <input type="hidden" name="uid">
            </form>
        </div>
    </div>
    <div style="height: 50%">
        <table id="dg"></table>
        <div id="tb">
            <input id="searchComment" type="text" placeholder="请输入搜索的评论">
            <a onclick="searchComment()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
            <a onclick="showUpdateDialog()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改评论</a>
            <a onclick="removeComment()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除评论</a>
        </div>
        <div id="update-dialog" closed="true" class="easyui-dialog" title="My Dialog" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
            <form id="update-form">
                评论内容：<textarea name="cmcontent" ></textarea>
                <input type="hidden" name="cid">
            </form>
        </div>
    </div>
</body>
</html>
