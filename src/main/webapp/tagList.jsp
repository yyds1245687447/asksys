<%--
  Created by IntelliJ IDEA.
  User: 008
  Date: 2022/7/4
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>分类管理</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function () {
            $('#dg').datagrid({
                pagination:true,
                toolbar:'#tb',
                fit:true,
                fitColumns:true,
                url:'/tags/listTags',
                columns:[[
                    {field:'tid',checkbox:true},
                    {field:'tname',title:'分类名称',width:100},
                    {field:'ctime',title:'创建时间',width:100},
                    {field:'mtime',title:'修改时间',width:100}
                ]]
            });
        })

        function showAddDiglog(){
            $("#add-diglog").dialog({
                closed: false,
                buttons:[{
                    text:'增加类别',
                    iconCls:'icon-save',
                    handler:function(){
                        $('#add-form').form('submit',{
                            url:'/tags/saveTags',
                            onSubmit: function(){
                                return $('#add-form').form('validate');
                            },
                            success:function(data){
                                if(data){
                                    $("#add-diglog").dialog({
                                        closed: true
                                    })
                                    $("#dg").datagrid('reload')
                                    $('#add-form').form('clear')
                                }else {
                                    alert('失败');
                                }
                            }
                        });
                    }
                },{
                    text:'重置数据',
                    iconCls:'icon-redo',
                    handler:function(){
                        $('#add-form').form('clear')
                    }
                }]
            })
        }
        
        function removeTags() {
            const arr = $('#dg').datagrid('getSelections');
            if(arr.length == 0){
                $.messager.alert('错误','至少选择一个数据','error');
            }else {
                var flag = confirm("是否确认删除");
                if(flag){
                    var ids = [];
                    for (const a in arr) {
                        ids.push(arr[0].tid);
                    }
                    $.post('/tags/removeTags',{'ids[]':ids},function (flag) {
                        if(flag){
                            $("#dg").datagrid('reload')
                            $.messager.show({
                                title:'温馨提示',
                                msg:'删除成功',
                                timeout:3000,
                                showType:'slide'
                            });
                        }else {
                            alert('删除失败');
                        }
                    })
                }
            }
        }
        
        function showUpdateTags() {
            const arr = $('#dg').datagrid('getSelections');
            if(arr.length!=1){
                $.messager.alert('错误','请选择一个','error');
                $('#dg').datagrid('unselectAll');
            }else {
                $('#update-form').form('load',{
                    tname:arr[0].tname,
                    mtime:arr[0].mtime,
                    tid:arr[0].tid
                })
                $("#update-diglog").dialog({
                    closed: false,
                    buttons:[{
                        text:'修改类别',
                        iconCls:'icon-edit',
                        handler:function(){
                            $('#update-form').form('submit',{
                                url:'/tags/updateTags',
                                onSubmit: function(){
                                    return $('#update-form').form('validate');
                                },
                                success:function(data){
                                    if(data){
                                        $("#update-diglog").dialog({
                                            closed: true
                                        })
                                        $("#dg").datagrid('reload')
                                        $('#update-form').form('clear')
                                        $.messager.show({
                                            title:'温馨提示',
                                            msg:'修改成功',
                                            timeout:3000,
                                            showType:'slide'
                                        });
                                    }else {
                                        $.messager.alert('错误','提交失败','error');
                                    }
                                }
                            });
                        }
                    },{
                        text:'重置数据',
                        iconCls:'icon-redo',
                        handler:function(){
                            $('#update-form').form('load',{
                                tname:arr[0].tname,
                                mtime:arr[0].mtime,
                                tid:arr[0].tid
                            })
                        }
                    }]
                })
            }
        }

        function searchTname() {
            let tname = $('#searchTname').val();
            $('#dg').datagrid('reload',{
                tname : tname
            })
        }
    </script>
</head>
<body>
<table id="dg"></table>
<div id="tb">
    <input id="searchTname" type="text" placeholder="请输入要搜索的内容">
    <a onclick="searchTname()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
    <a onclick="showAddDiglog()" href="#" class="easyui-linkbutton">增加类别</a>
    <a onclick="showUpdateTags()" href="#" class="easyui-linkbutton">修改类别</a>
    <a onclick="removeTags()" href="#" class="easyui-linkbutton">删除类别</a>
</div>
<div id="add-diglog" closed="true" class="easyui-dialog" title="增加类别" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="add-form">
        <p style="display: inline;margin-right: 10px">类别名称:</p>
        <input name="tname" class="easyui-textbox" required="required" style="width:300px">
        <br><br><br>
        <p style="display: inline;margin-right: 10px">创建时间:</p>
        <input name="ctime" id="dd" type="text" class="easyui-datetimebox" required="required" style="width:300px;margin-left: 20px">
    </form>
</div>
<div id="update-diglog" closed="true" class="easyui-dialog" title="修改类别" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="update-form">
        <p style="display: inline;margin-right: 10px">类别名称:</p>
        <input name="tname" class="easyui-textbox" required="required" style="width:300px">
        <br><br><br>
        <p style="display: inline;margin-right: 10px">修改时间:</p>
        <input name="mtime"  type="text" class="easyui-datetimebox" required="required" style="width:300px;margin-left: 20px">
        <input type="hidden" name="tid">
    </form>
</div>
</body>
</html>
