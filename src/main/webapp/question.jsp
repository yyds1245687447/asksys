<%--
  Created by IntelliJ IDEA.
  User: 008
  Date: 2022/7/4
  Time: 14:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>问题管理</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function () {
            $('#dg').datagrid({
                pagination: true,
                toolbar: '#tb',
                fit: true,
                fitColumns: true,
                url: '/question/listQuestion',
                columns: [[
                    {field: 'qid', checkbox: true},
                    {field: 'uname', title: '用户名称', width: 100},
                    {field: 'tname', title: '分类名称', width: 100},
                    {field: 'title', title: '问题标题', width: 100},
                    {field: 'content', title: '问题内容', width: 100},
                    {field: 'ctime', title: '创建时间', width: 100},
                    {field: 'mtime', title: '修改时间', width: 100},
                    {
                        field: 'abc', title: '回复问题', width: 100, formatter: function (value, row, index) {
                            return "<input onclick='answerQuestion(" + row.qid + ")' type='button' value='回复问题'";
                        }
                    }
                ]]
            });
        })

        function showAddDiglog() {
            $("#add-diglog").dialog({
                closed: false,
                buttons: [{
                    text: '增加问题',
                    iconCls: 'icon-save',
                    handler: function () {
                        $('#add-form').form('submit', {
                            url: '/question/saveQuestion',
                            onSubmit: function () {
                                return $('#add-form').form('validate');
                            },
                            success: function (data) {
                                if (data) {
                                    $("#add-diglog").dialog({
                                        closed: true
                                    })
                                    $.messager.show({
                                        title: '温馨提示',
                                        msg: '增加成功',
                                        timeout: 3000,
                                        showType: 'slide'
                                    });
                                    $("#dg").datagrid('reload')
                                    $('#add-form').form('clear')
                                } else {
                                    $.messager.alert('错误', '增加失败', 'error');
                                }
                            }
                        });
                    }
                }, {
                    text: '重置数据',
                    iconCls: 'icon-redo',
                    handler: function () {
                        $('#add-form').form('clear')
                    }
                }]
            })
        }

        function showUpdateTags() {
            const arr = $('#dg').datagrid('getSelections');
            if (arr.length != 1) {
                $.messager.alert('错误', '请选择一个', 'error');
                $('#dg').datagrid('unselectAll');
            } else {
                $('#update-form').form('load', {
                    title: arr[0].title,
                    content: arr[0].content,
                    mtime: arr[0].mtime,
                    qid: arr[0].qid
                })
                $("#update-diglog").dialog({
                    closed: false,
                    buttons: [{
                        text: '修改问题',
                        iconCls: 'icon-edit',
                        handler: function () {
                            $('#update-form').form('submit', {
                                url: '/question/updateQuestion',
                                onSubmit: function () {
                                    return $('#update-form').form('validate');
                                },
                                success: function (data) {
                                    if (data!=0) {
                                        $("#update-diglog").dialog({
                                            closed: true
                                        })
                                        $("#dg").datagrid('reload')
                                        $('#update-form').form('clear')
                                        $.messager.show({
                                            title: '温馨提示',
                                            msg: '修改成功',
                                            timeout: 3000,
                                            showType: 'slide'
                                        });
                                    } else {
                                        $.messager.alert('错误', '只能修改自己提出的问题', 'error');
                                    }
                                }
                            });
                        }
                    }, {
                        text: '重置数据',
                        iconCls: 'icon-redo',
                        handler: function () {
                            $('#update-form').form('load', {
                                title: arr[0].title,
                                content: arr[0].content,
                                mtime: arr[0].mtime,
                                qid: arr[0].qid
                            })
                        }
                    }]
                })
            }
        }

        function removeQuestion() {

            const arr = $('#dg').datagrid('getSelections');
            if (arr.length == 0) {
                $.messager.alert('错误', '至少选择一个数据', 'error');
            } else {
                var flag = confirm("是否确认删除");
                if (flag) {
                    var ids = [];
                    for (const a in arr) {
                        ids.push(arr[0].qid);
                    }
                    $.post('/question/removeQuestion', {'ids[]': ids}, function (flag) {
                        if (flag) {
                            $.messager.show({
                                title: '温馨提示',
                                msg: '删除成功',
                                timeout: 3000,
                                showType: 'slide'
                            });
                            $("#dg").datagrid('reload')
                        } else {
                            $.messager.alert('错误', '只能删除自己提出的问题', 'error');
                        }
                    })
                }
            }
        }

        function searchTitle() {
            let title = $('#searchTitle').val();
            $('#dg').datagrid('reload', {
                title: title
            })
        }

        function answerQuestion(qid) {
            $('#answer-form').form('load', {
                qid:qid
            })
            $("#answer-diglog").dialog({
                closed: false,
                buttons: [{
                    text: '回复问题',
                    iconCls: 'icon-save',
                    handler: function () {
                        $('#answer-form').form('submit', {
                            url: '/answer/saveAnswer',
                            onSubmit: function () {
                                return $('#answer-form').form('validate');
                            },
                            success: function (data) {
                                if (data) {
                                    $("#answer-diglog").dialog({
                                        closed: true,
                                    })
                                    $.messager.show({
                                        title: '温馨提示',
                                        msg: '回复成功',
                                        timeout: 3000,
                                        showType: 'slide'
                                    });
                                }else{
                                    $.messager.alert('错误', '回复失败', 'error');
                                }
                            }
                        })
                    }
                }, {
                    text: '重置数据',
                    iconCls: 'icon-redo',
                    handler: function () {
                        $('#update-form').form('load', {
                            title: arr[0].title,
                            content: arr[0].content,
                            mtime: arr[0].mtime,
                            qid: arr[0].qid
                        })
                    }
                }]
            })
        }
    </script>
</head>
<body>
<table id="dg"></table>
<div id="tb">
    <input id="searchTitle" type="text" placeholder="请输入要搜索的内容">
    <a onclick="searchTitle()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
    <a onclick="showAddDiglog()" href="#" class="easyui-linkbutton">增加问题</a>
    <a onclick="showUpdateTags()" href="#" class="easyui-linkbutton">修改问题</a>
    <a onclick="removeQuestion()" href="#" class="easyui-linkbutton">删除问题</a>
</div>
<div id="add-diglog" closed="true" class="easyui-dialog" title="增加问题" style="width:400px;height:240px;"
     data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="add-form">
        <p style="display: inline;margin-right: 10px">问题类别:</p>
        <input id="cc" class="easyui-combobox" name="tid"
               data-options="valueField:'tid',textField:'tname',url:'/tags/ListSelectTags'">
        <br><br>
        <p style="display: inline;margin-right: 10px">问题标题:</p>
        <input name="title" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">问题内容:</p>
        <textarea name="content"></textarea>
    </form>
</div>
<div id="update-diglog" closed="true" class="easyui-dialog" title="修改问题" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="update-form">
        <p style="display: inline;margin-right: 10px">问题类别:</p>
        <input id="c" class="easyui-combobox" name="tid"
               data-options="valueField:'tid',textField:'tname',url:'/tags/ListSelectTags'">
        <br><br>
        <p style="display: inline;margin-right: 10px">问题标题:</p>
        <input name="title" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">问题内容:</p>
        <input name="content" class="easyui-textbox" required="required" style="width:300px">
        <br><br>
        <input type="hidden" name="qid">
        <input type="hidden" name="tid">
    </form>
</div>
<div id="answer-diglog" closed="true" class="easyui-dialog" title="回复问题" style="width:400px;height:240px;">
    <form id="answer-form">
        <input name="content" class="easyui-textbox" required="required" style="width:300px">
        <input type="hidden" name="qid">
    </form>
</div>
</body>
</html>
