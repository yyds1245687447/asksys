<%--
  Created by IntelliJ IDEA.
  User: 008
  Date: 2022/7/6
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function () {
            $("#login-diglog").dialog({
                buttons: [{
                    text: '登录',
                    iconCls: 'icon-ok',
                    handler:function(){
                        $('#login-form').form('submit',{
                            url:'/user/login',
                            onSubmit: function(){
                                return $('#login-form').form('validate');
                            },
                            success:function(data){
                                if(data==1){
                                    window.location.href="index.jsp"
                                }else if(data==0){
                                    $.messager.alert('错误','登陆失败','error');
                                }
                            }
                        });
                    }
                },{
                    text: '注册',
                    iconCls: 'icon-ok',
                    handler:function(){
                        $("#save-diglog").dialog({
                            closed: false,
                            buttons: [{
                                text: '提交',
                                iconCls: 'icon-ok',
                                handler:function(){
                                    var pw1 = $('#a').val();
                                    var pw2 = $('#b').val();
                                    if(pw1!=pw2){
                                        $.messager.alert('错误','两次输入的密码不一样','error');
                                        $("#save-diglog").dialog({
                                            closed: true,
                                        })
                                    }else {
                                        $('#add-form').form('submit',{
                                            url:'/user/save',
                                            onSubmit: function(){
                                                return $('#add-form').form('validate');
                                            },
                                            success:function(data){
                                                if(data==0){
                                                    $("#save-diglog").dialog({
                                                        closed: true,
                                                    })
                                                    $.messager.show({
                                                        title: '温馨提示',
                                                        msg: '注册成功',
                                                        timeout: 3000,
                                                        showType: 'slide'
                                                    });
                                                }else if(data==1){
                                                    $.messager.alert('错误','注册失败：用户账号重复','error');
                                                }
                                            }
                                        })
                                    }
                                }
                            }]
                        })
                    }
                }]
            })
        })
    </script>
</head>
<body>
<div id="login-diglog" class="easyui-dialog" title="登录" style="width:400px;height:240px;" modal="true">
    <div style="position: relative;top: 20%;">
        <form id="login-form" method="post">
            <p style="display: inline;margin-right: 10px;margin-left: 15px;">用户名:</p>
            <input class="easyui-textbox" name="uname" required="true" data-options="iconCls:'icon-man'" style="width:300px">
            <br><br><br>
            <p style="display: inline;margin-right: 22px;margin-left: 15px;">密码:</p>
            <input class="easyui-passwordbox" name="password" required="true" data-options="iconCls:'icon-lock'" style="width:300px;">
        </form>
    </div>
</div>
<div id="save-diglog" closed="true" class="easyui-dialog" title="注册用户" style="width:400px;height:240px;"
     data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="add-form">
        <p style="display: inline;margin-right: 10px">用户账号:</p>
        <input class="easyui-textbox" name="uname" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">用户密码:</p>
        <input id="a" name="password" class="easyui-passwordbox" required="required" style="width:300px">
        <br><br>
        <p style="display: inline;margin-right: 10px">确认密码:</p>
        <input id="b" class="easyui-passwordbox" required="required" style="width:300px">
    </form>
</div>
</body>
</html>
