<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2022/7/7
  Time: 11:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Answer</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function () {
            $('#dg').datagrid({
                pagination: true,
                fit: true,
                toolbar: '#tb',
                fitColumns: true,
                url: '/answer/listAnswer',//controller路径
                //field:对应数据库列名，title:表格标题
                columns: [[
                    {field: 'aid', checkbox: true},
                    {field: 'cid', title: '评论名', width: 50},
                    {field: 'uid', title: '提问题用户名', width: 50},
                    {field: 'content', title: '回复内容', width: 50},
                    {field: 'commentNum', title: '内容数', width: 50},
                    {field: 'agree_num', title: '点赞数', width: 50},
                    {field: 'ctime', title: '回复时间', width: 50, align: 'right'},
                    {field: 'mtime', title: '修改时间', width: 50, align: 'right'},
                    {field: 'uname', title: '回复人姓名', width: 50, align: 'right'},
                    {field: 'title', title: '回复问题标题', width: 50, align: 'right'},
                ]]
            });
        })

        function updateAnswer(){
            var arr = $('#dg').datagrid('getSelections');
            if (arr.length != 1) {
                //alert("请选择一行");
                $.messager.alert('警告', '请选择一行', 'warning');
                $('#dg').datagrid('unselectAll')
            } else {

                $('#update-form').form('load', {
                    content: arr[0].content,
                    aid: arr[0].aid,
                    uid:arr[0].uid
                })

                $("#update-dialog").dialog({
                    closed: false,
                    buttons: [{
                        text: '修改回复',
                        iconCls: 'icon-edit',
                        handler: function () {
                            $('#update-form').form('submit', {
                                url: "/answer/updateAnswer",
                                onSubmit: function () {
                                    return $('update-form').form('validate');
                                },
                                //提交成功以后的操作，data为返回值类型
                                success: function (data) {
                                    if (data==1) {
                                        $("#update-dialog").dialog({
                                            closed: true,
                                        })
                                        $('#dg').datagrid('reload')
                                        $('#update-dialog').form('clear')
                                        $.messager.show({
                                            title: '温馨提示',
                                            msg: '提交成功',
                                            timeout: 1000,
                                            showType: 'slide'
                                        });
                                    } else {
                                        alert("提交失败,不能修改其他人的评论");
                                    }

                                }
                            });
                        }
                    }, {
                        text: '重置数据',
                        iconCls: 'icon-redo',
                        handler: function () {
                            $('#update-form').form('load', {
                                title: arr[0].title,
                                tid: arr[0].tid,
                                content: arr[0].content,
                                qid: arr[0].qid
                            })
                        }
                    }]

                })
            }
        }

        function removeAnswer() {
            var arr = $('#dg').datagrid('getSelections');
            if (arr.length == 0) {
                alert("请至少选择一行数据")
            } else {
                var flag = confirm("是否确认删除");
                //  alert("flag"+flag)
                if (flag == true) {
                    var ids = [];
                    for (var a of arr) {
                        ids.push(a.aid);
                    }
                    $.post('/answer/removeAnswer', {'ids[]': ids}, function (flag) {
                        if (flag) {
                            //刷新一下
                            $('#dg').datagrid('reload')
                        } else {
                            alert("删除失败，只能删除自己的评论");
                        }
                    });
                }
            }
        }

        function likesAnswer(){
            var arr = $('#dg').datagrid('getSelections');
            if (arr.length != 1) {
                $.messager.alert('警告', '请选择一行', 'warning');
                $('#dg').datagrid('unselectAll')
            } else{
                var flag=confirm("是否点赞");
                if (flag==true){
                    $.post('/answer/likesAnswer',{'aid':arr[0].aid},function (flag){
                        if (flag) {
                            //刷新一下
                            $('#dg').datagrid('reload')
                        } else {
                            alert("删除失败");
                        }
                    });
                }

            }
        }
    </script>
</head>
<body>
<table id="dg"></table>
<div id="tb">
    <a onclick="likesAnswer()" href="#" class="easyui-linkbutton"
       data-options="iconCls:'icon-add',plain:true">点赞回复</a>
    <a onclick="updateAnswer()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改回复</a>
    <a onclick="removeAnswer()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除回复</a>
</div>

<div id="update-dialog" closed="true" class="easyui-dialog" title="修改回复" style="width:400px;height:200px;"
     data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="update-form">
        回复内容：<textarea name="content"></textarea>
        <input type="hidden" name="aid">
    </form>
</div>

</body>
</html>
