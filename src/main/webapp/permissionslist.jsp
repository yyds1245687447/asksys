<%--
  Created by IntelliJ IDEA.
  User: 86152
  Date: 2022/7/7
  Time: 23:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="jeasyui/themes/icon.css">
    <script type="text/javascript" src="jeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="jeasyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="jeasyui/easyui-lang-zh_CN.js"></script>
    <script>
        $(function (){
            $('#dg').datagrid({
                pagination:true,
                toolbar:'#tb',
                fit:true,
                fitColumns:true,
                url:'/permissions/listPermissions',
                columns:[[
                    {field:'pid',checkbox:true},
                    {field:'pname',title:'权限名称',width:100},
                    {field:'pdesc',title:'权限描述',width:100,align:'right'}
                ]]
            });
        })

        function showAddDialog(){
            $("#add-dialog").dialog({
                closed:false,
                buttons:[{
                    text:'增加权限',
                    iconCls:'icon-save',
                    //当点击按钮时执行大括号中的代码
                    handler:function(){
                        //$('#add-form').form表单
                        $('#add-form').form('submit',{
                            //请求的路径
                            url:'/permissions/savaPermissions',
                            //提交的时候执行的操作
                            onSubmit: function(){
                                return $('#add-form').form('validate');
                            },
                            //提交成功以后执行的操作，data表示返回值类型
                            success:function(data){
                                //判断是否提交成功
                                if(data){
                                    //让对话框消失
                                    $("#add-dialog").dialog({
                                        closed:true
                                    })
                                    //自动刷新表格
                                    $('#dg').datagrid('reload')
                                    //表单会默认缓存数据，需要清空数据
                                    $('#add-form').form('clear')
                                }else{
                                    alert("提交失败");
                                }
                            }
                        });
                    }
                },{
                    text:'重置数据',
                    iconCls: 'icon-redo',
                    handler:function(){
                        $('#add-form').form('clear')
                    }
                }]
            })
        }

        function removePermissions(){
            //判断用户是否选择的数据
            //获取用户选择的数据，并保存到数组中
            var arr = $("#dg").datagrid('getSelections');
            /*for(var a of arr){
                alert(a.tname);
            }*/
            //如果数组长度为0，说明用户没有选择数据
            if(arr.length==0){
                alert("请至少选择一行");
            }else{
                //删除必须添加确认框
                var flag=confirm("是否确认删除");
                //alert("flag="+flag);
                if(flag==true){
                    //执行删除操作
                    //创建一个空数组
                    var ids=[];
                    for(var a of arr){
                        //把每个tid放到ids中
                        ids.push(a.pid);
                    }
                    //alert(ids);
                    //向后台发送数据,第一个参数表示请求的路径，第二个参数表示向后台发送的数据
                    //第三个参数是回调函数，其中flag表示提交成功以后Controller返回的值
                    $.post('/permissions/removePermissions',{'ids[]':ids},function (flag){
                        if(flag){
                            //刷新表格
                            $('#dg').datagrid('reload')
                        }else{
                            alert("删除失败");
                        }
                    });
                }
            }
        }

        function showUpdateDialog(){
            //获取用户选择的行
            var arr=$("#dg").datagrid('getSelections');
            if(arr.length!=1){
                $.messager.alert('警告','请选择一行','warning');
                $("#dg").datagrid('unselectAll');
            }else{
                // alert("选择正确");
                //回显数据
                $('#update-form').form('load',{
                    pname:arr[0].pname,
                    pdesc:arr[0].pdesc,
                    pid:arr[0].pid
                })
                //显示对话框
                $("#update-dialog").dialog({
                    closed:false,
                    buttons:[{
                        text:'修改权限',
                        iconCls:'icon-edit',
                        //当点击按钮时执行大括号中的代码
                        handler:function(){
                            //$('#add-form').form表单
                            $('#update-form').form('submit',{
                                //请求的路径
                                url:'/permissions/updatePermissions',
                                //提交的时候执行的操作
                                onSubmit: function(){
                                    return $('#update-form').form('validate');
                                },
                                //提交成功以后执行的操作，data表示返回值类型
                                success:function(data){
                                    //判断是否提交成功
                                    if(data){
                                        //让对话框消失
                                        $("#update-dialog").dialog({
                                            closed:true
                                        })
                                        //自动刷新表格
                                        $('#dg').datagrid('reload')
                                        //表单会默认缓存数据，需要清空数据
                                        $('#update-form').form('clear')
                                        $.messager.show({
                                            title:'温馨提示',
                                            msg:'修改成功',
                                            //时间单位是毫秒
                                            timeout:3000,
                                            showType:'slide'
                                        });
                                    }else{
                                        alert("提交失败");
                                    }
                                }
                            });
                        }
                    },{
                        text:'重置数据',
                        iconCls: 'icon-redo',
                        handler:function(){
                            //如果给修改重置，要恢复到修改以前的状态
                            $('#update-form').form('load',{
                                pname:arr[0].pname,
                                pdesc:arr[0].pdesc,
                                pid:arr[0].pid
                            })
                        }
                    }]
                })
            }
        }

        function searchPname(){
            //把tname提交给后台
            var pname=$("#searchPname").val();
            //alert(tname);
            $("#dg").datagrid('reload',{
                pname:pname
            })
        }
    </script>
</head>
<body>
<table id="dg"></table>
<div id="tb">
    <input id="searchPname" type="text" placeholder="请输入要搜索的权限">
    <a onclick="searchPname()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true">搜索</a>
    <a onclick="showAddDialog()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">增加权限</a>
    <a onclick="showUpdateDialog()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true">修改权限</a>
    <a onclick="removePermissions()" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除权限</a>
</div>

<div id="add-dialog" closed="true" class="easyui-dialog" title="增加权限" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="add-form">
        权限名称：<input name="pname" class="easyui-validatebox" data-options="required:true">
        <br/><br/><br/>
        权限内容：<input  name="pdesc" data-options="required:true,showSeconds:false" >
    </form>
</div>

<div id="update-dialog" closed="true" class="easyui-dialog" title="修改权限" style="width:400px;height:200px;" data-options="iconCls:'icon-save',resizable:true,modal:true">
    <form id="update-form">
        权限名称：<input name="pname" class="easyui-validatebox" data-options="required:true">
        <br/><br/><br/>
        权限内容：<input  name="pdesc" data-options="required:true,showSeconds:false" >
        <input name="pid" hidden="true">
    </form>
</div>
</body>
</html>
