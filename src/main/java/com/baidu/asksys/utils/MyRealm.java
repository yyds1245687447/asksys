package com.baidu.asksys.utils;


import com.baidu.asksys.bean.User;
import com.baidu.asksys.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;


//登录认证类
public class MyRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    public User login(String uname) {
        return userService.login(uname);
    }

    //处理用户登录用的
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String username = (String) token.getPrincipal();
        SimpleAuthenticationInfo info = null;
        User user = login(username);
        if (null != user) {
            //登录成功把用户名存到session中
            SecurityUtils.getSubject().getSession().setAttribute("user", user);
            info = new SimpleAuthenticationInfo(user.getUname(), user.getPassword(), super.getName());
        }
        return info;
    }

    //处理用户角色权限
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }


}
