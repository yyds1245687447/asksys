package com.baidu.asksys.dao;

import com.baidu.asksys.bean.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    User login(String uname);

    int save(@Param("uname") String uname, @Param("password") String password);

    List<User> list(@Param("page") int page, @Param("rows") int rows, @Param("uname") String uname);

    int count();

    int remove(@Param("list") List<Integer> ids, @Param("uid") int uid);

    int update(User user);
}
