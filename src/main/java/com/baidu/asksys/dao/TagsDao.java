package com.baidu.asksys.dao;

import com.baidu.asksys.bean.Tags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository

public interface TagsDao {
    //    @Param()给sql传值
    List<Tags> list(@Param("page") int page, @Param("rows") int rows, @Param("tname") String tname);

    int count();

    int save(Tags tags);

    int remove(@Param("list") List<Integer> ids);

    int update(Tags tags);

    List<Tags> ListSelectTags();

}

