package com.baidu.asksys.dao;

import com.baidu.asksys.bean.Permissions;
import com.baidu.asksys.bean.Tags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionsDao {
    List<Permissions> list(@Param("page") int page, @Param("rows") int rows, @Param("pname") String pname);

    int save(Permissions permissions);

    int remove(List<Integer> ids);

    //修改问题分类，参数是分类对象
    int update(Permissions permissions);

    int count();
}
