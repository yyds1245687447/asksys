package com.baidu.asksys.dao;

import com.baidu.asksys.bean.Answer;
import com.baidu.asksys.bean.Question;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerDao {
    int count(@Param("title") String title);

    List<Answer> list(@Param("page") int page, @Param("rows") int rows, @Param("title") String title);

    int save(Answer answer);

    int update(Answer answer);

    int remove(@Param("list") List<Integer> ids,@Param("uid") int uid);

    int likes(int aid);
}
