package com.baidu.asksys.dao;

import com.baidu.asksys.bean.Comment;
import com.baidu.asksys.bean.Tags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CommentDao {
    List<Comment> list(@Param("page") int page, @Param("rows") int rows, @Param("cmcontent") String cmcontent);
    //查询数目
    int count(@Param("content") String content);
    //插入
    int save(Comment comment);
    //删除多条数据
    int remove(@Param("list") List<Integer> ids,@Param("uid") int uid);
    //更新
    int update(Comment comment);
}
