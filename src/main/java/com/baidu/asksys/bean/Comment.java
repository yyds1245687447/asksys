package com.baidu.asksys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment  implements Serializable {
    private Integer cid;//评论id
    private Integer uid;//用户id
    private String cmcontent;//评论内容
    private String ctime;//创建时间
    private String mtime;//修改时间
    private Integer del;//删除标记
    private Integer qid;
    private String uname;
    private String tname;
    private String content;//问题内容
}
