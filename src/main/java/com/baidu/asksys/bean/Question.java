package com.baidu.asksys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    private Integer qid;
    private Integer uid;
    private Integer tid;
    private Integer aid;
    private String title;
    private String content;
    private String ctime;
    private String mtime;
    private Integer del;

    private String uname;
    private String tname;
}
