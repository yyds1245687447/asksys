package com.baidu.asksys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer implements Serializable {

    private Integer aid;
    private Integer cid;
    private Integer uid;
    private String content;
    private Integer comment_num;
    private Integer agree_num;
    private String ctime;
    private String mtime;
    private Integer del;
    private Integer qid;
}
