package com.baidu.asksys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private Integer uid;
    private String uname;
    private String nickname;
    private String password;
    private String sex;
    private String avatar;
    private String remark;
    private String ctime;
    private String mtime;
    private Integer del;
}
