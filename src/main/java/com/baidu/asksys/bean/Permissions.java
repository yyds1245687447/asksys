package com.baidu.asksys.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permissions {
    private Integer pid;//权限id
    private String pname;//权限名称
    private String pdesc;//权限描述
}
