package com.baidu.asksys.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tags {
    private Integer tid;
    private String tname;
    private String ctime;
    private String mtime;
}

