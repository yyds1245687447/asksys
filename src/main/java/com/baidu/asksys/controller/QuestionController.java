package com.baidu.asksys.controller;

import com.baidu.asksys.bean.Question;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.service.QuestionService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @RequestMapping("/listQuestion")
    public Map<String, Object> listQuestion(String page, String rows, String title) {
        Map<String, Object> map = new HashMap<>();
        map.put("total", questionService.count(title));
        map.put("rows", questionService.listQuestion(page, rows, title));
        return map;
    }

    @RequestMapping("/saveQuestion")
    public boolean saveQuestion(Question question) {
        return questionService.save(question);
    }

    @RequestMapping("/removeQuestion")
    public boolean removeQuestion(@RequestParam("ids[]") List<Integer> ids) {
        return questionService.remove(ids);
    }

    @RequestMapping("/updateQuestion")
    public int updateQuestion(Question question) {
        User users = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        if (users.getUid() != question.getUid()) {
            return 0;
        }
        return questionService.update(question);
    }
}
