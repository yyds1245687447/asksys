package com.baidu.asksys.controller;

import com.baidu.asksys.bean.Comment;
import com.baidu.asksys.bean.Question;
import com.baidu.asksys.bean.Tags;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.service.CommentService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @RequestMapping("/listComment")
    public Map<String,Object> listComment(String page, String rows, String cmcontent){
        //前端需要总记录数，必须要保存到tatal
        //需要查询数据保存rows
        // System.out.println("当前页数"+page+"每页记录数"+rows);
        Map<String,Object> map=new HashMap<>();
        map.put("total",commentService.countComment(cmcontent));
        map.put("rows",commentService.listComment(page,rows,cmcontent));
        return map;
    }
    @RequestMapping("saveComment")
    public boolean saveComment(Comment comment){
        User users=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        comment.setUid(users.getUid());
        //System.out.println(comment.getUid()+","+comment.getTid()+","+comment.getContent());
        return commentService.saveComment(comment);
    }
    @RequestMapping("/updateComment")
    public int updateComment(Comment comment){
        User users=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        comment.setUid(users.getUid());
        return commentService.updateComment(comment);
    }
    @RequestMapping("/removeComment")//ids是前端数组与前端名字要一致
    public boolean removeQuestion(@RequestParam("ids[]") List<Integer> ids){
        System.out.println(ids);
        return commentService.removeComment(ids);

    }
}
