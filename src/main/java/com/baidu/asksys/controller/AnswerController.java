package com.baidu.asksys.controller;


import com.baidu.asksys.bean.Answer;
import com.baidu.asksys.bean.Question;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.service.AnswerService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/answer")
public class AnswerController {
    @Autowired
    private AnswerService answerService;

    @RequestMapping("/saveAnswer")
    public boolean saveAnswer(Answer answer){
        return answerService.saveAnswer(answer);
    }

    @RequestMapping("/listAnswer")
    public Map<String, Object> listAnswer(String page, String rows, String title) {
        Map<String, Object> map = new HashMap<>();
        map.put("total", answerService.count(title));
        map.put("rows", answerService.listQuestion(page, rows, title));
        return map;
    }

    @RequestMapping("/updateAnswer")
    public int updateAnswer(Answer answer) {
        if (answerService.update(answer)){
            return 1;
        }
        return 0;
    }

    @RequestMapping("/removeAnswer")
    public boolean removeAnswer(@RequestParam("ids[]") List<Integer> ids) {
        return answerService.remove(ids);
    }

    @RequestMapping("/likesAnswer")
    public boolean likesAnswer(Answer answer) {
        return answerService.likes(answer.getAid());
    }
}
