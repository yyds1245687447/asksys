package com.baidu.asksys.controller;

import com.baidu.asksys.bean.Tags;
import com.baidu.asksys.service.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tags")
public class TagsController {

    @Autowired
    private TagsService tagsService;

    @RequestMapping("/listTags")
    public Map<String, Object> listTags(String page, String rows, String tname) {

        Map<String, Object> map = new HashMap<>();
        map.put("total", tagsService.count());
        map.put("rows", tagsService.listTags(page, rows, tname));
        return map;
    }

    @RequestMapping("/saveTags")
    public boolean saveTags(Tags tags) {
        return tagsService.saveTags(tags);
    }

    @RequestMapping("/removeTags")
    public boolean removeTags(@RequestParam("ids[]") List<Integer> ids) {
        return tagsService.removeTags(ids);
    }

    @RequestMapping("/updateTags")
    public boolean updateTags(Tags tags) {
        return tagsService.updateTags(tags);
    }

    @RequestMapping("/ListSelectTags")
    public List<Tags> ListSelectTags() {
        return tagsService.ListSelectTags();
    }
}
