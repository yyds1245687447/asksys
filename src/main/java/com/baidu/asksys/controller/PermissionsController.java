package com.baidu.asksys.controller;


import com.baidu.asksys.bean.Permissions;
import com.baidu.asksys.bean.Tags;
import com.baidu.asksys.service.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController

@RequestMapping("/permissions")
public class PermissionsController {
    @Autowired
    private PermissionsService permissionsService;

    @RequestMapping("/listPermissions")
    public Map<String,Object> listTags(String page, String rows, String pname){
        //前端需要总记录数，必须保存到total
        //需要查询数据保存rows
        //使用Map保存
        Map<String,Object> map = new HashMap<>();
        map.put("total",permissionsService.countPermissions());
        map.put("rows",permissionsService.listPermissions(page,rows,pname));
        return map;
    }

    @RequestMapping("/savaPermissions")
    public boolean savaPermissions(Permissions permissions){

        return permissionsService.savePermissions(permissions);
    }

    @RequestMapping("/removePermissions")
    public boolean removePermissions(@RequestParam("ids[]") List<Integer> ids){
        return permissionsService.removePermissions(ids);
    }

    //更新数据
    @RequestMapping("/updatePermissions")
    public boolean updatePermissions(Permissions permissions){
        return permissionsService.updatePermissions(permissions);
    }
}
