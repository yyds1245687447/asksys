package com.baidu.asksys.controller;

import com.baidu.asksys.bean.User;
import com.baidu.asksys.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public int login(String uname, String password) {

//        密码加密
        Md5Hash md5Hash = new Md5Hash(password, uname + password, 3);
        String s = md5Hash.toString();
//        获取令牌对象
        UsernamePasswordToken Token = new UsernamePasswordToken(uname, s);
//        获取认证对象
        Subject subject = SecurityUtils.getSubject();
//        如果用户没有认证，需要登录
        try {
            if (!subject.isAuthenticated()) {
                subject.login(Token);
            }
        } catch (AuthenticationException e) {
//            出现异常，登陆失败
            return 0;
        }
//        登陆成功
        return 1;
    }

    @RequestMapping("/save")
    public int saveLogin(String uname, String password) {
        Md5Hash md5Hash = new Md5Hash(password, uname + password, 3);
        String s = md5Hash.toString();
        int flag = 0;
        if(userService.save(uname, s)==false){
            flag=1;
        }
        return flag;
    }

    @RequestMapping("/listUser")
    public Map<String, Object> listUser(int page, int rows, String uname) {
        Map<String, Object> map = new HashMap<>();
        map.put("total", userService.count());
        map.put("rows", userService.listUser(page, rows, uname));
        return map;
    }

    @RequestMapping("/removeUser")
    public boolean removeQuestion(@RequestParam("ids[]") List<Integer> ids) {
        return userService.remove(ids);
    }

    @RequestMapping("/updateUser")
    public int updateQuestion(User user) {
        User users = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        user.setUname(users.getUname());
        if (user.getUid() != users.getUid()) {
            return 0;
        }
        Md5Hash md5Hash = new Md5Hash(user.getPassword(), user.getUname() + user.getPassword(), 3);
        user.setPassword(md5Hash.toString());
        return userService.update(user);
    }
}
