package com.baidu.asksys.service;

import com.baidu.asksys.bean.Tags;

import java.util.List;

public interface TagsService {
    List<Tags> listTags(String page, String rows, String tname);

    int count();

    boolean saveTags(Tags tags);

    boolean removeTags(List<Integer> ids);

    boolean updateTags(Tags tags);

    List<Tags> ListSelectTags();
}
