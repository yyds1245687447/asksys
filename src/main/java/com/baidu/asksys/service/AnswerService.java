package com.baidu.asksys.service;

import com.baidu.asksys.bean.Answer;
import com.baidu.asksys.bean.Question;

import java.util.List;

public interface AnswerService {
    boolean saveAnswer(Answer answer);

    int count(String title);

    List<Answer> listQuestion(String page, String rows, String title);

    boolean update(Answer answer);

    boolean remove(List<Integer> ids);

    boolean likes(int aid);
}
