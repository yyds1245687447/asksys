package com.baidu.asksys.service;

import com.baidu.asksys.bean.Permissions;
import com.baidu.asksys.bean.Tags;

import java.util.List;

public interface PermissionsService {
    List<Permissions> listPermissions(String page,String rows,String pname);

    boolean savePermissions(Permissions permissions);

    boolean removePermissions(List<Integer> ids);

    //修改
    boolean updatePermissions(Permissions permissions);

    int countPermissions();
}
