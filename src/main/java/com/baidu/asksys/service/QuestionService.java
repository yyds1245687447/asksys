package com.baidu.asksys.service;

import com.baidu.asksys.bean.Question;

import java.util.List;

public interface QuestionService {
    int count(String title);

    List<Question> listQuestion(String page, String rows, String uname);

    boolean save(Question question);

    boolean remove(List<Integer> ids);

    int update(Question question);
}
