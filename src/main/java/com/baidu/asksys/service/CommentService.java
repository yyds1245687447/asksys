package com.baidu.asksys.service;

import com.baidu.asksys.bean.Comment;
import com.baidu.asksys.bean.Tags;

import java.util.List;

public interface CommentService {
    //查询所有Tags
    List<Comment> listComment(String page, String rows, String cmcontent);
    int countComment(String cmcontent);
    boolean saveComment(Comment tags);
    boolean removeComment(List<Integer> ids);
    int updateComment(Comment tags);
    //给下拉列表提供数据
    List<Comment> listComment();
}
