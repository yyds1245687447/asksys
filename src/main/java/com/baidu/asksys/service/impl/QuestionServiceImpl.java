package com.baidu.asksys.service.impl;

import com.baidu.asksys.bean.Question;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.dao.QuestionDao;
import com.baidu.asksys.service.QuestionService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionDao questionDao;

    @Override
    public int count(String title) {
        return questionDao.count(title);
    }

    @Override
    public List<Question> listQuestion(String page, String rows, String uname) {
        int ps = Integer.parseInt(page);
        int rs = Integer.parseInt(rows);
        return questionDao.list((ps - 1) * rs, rs, uname);
    }

    @Override
    public boolean save(Question question) {
//        取session内容
        User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        question.setUid(user.getUid());
        return questionDao.save(question) > 0;
    }

    @Override
    public boolean remove(List<Integer> ids) {
        User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        return questionDao.remove(ids, user.getUid()) > 0;
    }

    @Override
    public int update(Question question) {
        return questionDao.update(question);
    }

}
