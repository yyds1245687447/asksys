package com.baidu.asksys.service.impl;


import com.baidu.asksys.bean.Tags;
import com.baidu.asksys.dao.TagsDao;
import com.baidu.asksys.service.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsServiceImpl implements TagsService {
    @Autowired
    private TagsDao tagsDao;

    @Override
    public List<Tags> listTags(String page, String rows, String tname) {
        int p = Integer.parseInt(page);
        int r = Integer.parseInt(rows);
        return tagsDao.list((p - 1) * r, r, tname);
    }

    public int count() {
        return tagsDao.count();
    }

    @Override
    public boolean saveTags(Tags tags) {
        return tagsDao.save(tags) > 0;
    }

    @Override
    public boolean removeTags(List<Integer> ids) {
        return tagsDao.remove(ids) > 0;
    }

    @Override
    public boolean updateTags(Tags tags) {
        return tagsDao.update(tags) > 0;
    }

    @Override
    public List<Tags> ListSelectTags() {
        return tagsDao.ListSelectTags();
    }
}
