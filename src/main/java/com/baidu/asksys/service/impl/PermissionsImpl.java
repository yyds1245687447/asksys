package com.baidu.asksys.service.impl;

import com.baidu.asksys.bean.Permissions;
import com.baidu.asksys.dao.PermissionsDao;
import com.baidu.asksys.service.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionsImpl implements PermissionsService {
    @Autowired
    private PermissionsDao permissionsDao;
    @Override
    public List<Permissions> listPermissions(String page,String rows,String pname) {
        int p = Integer.parseInt(page);
        int r = Integer.parseInt(rows);
        return permissionsDao.list((p-1)*r,r,pname);
    }

    @Override
    public boolean savePermissions(Permissions permissions) {
        return permissionsDao.save(permissions)>0;
    }

    @Override
    public boolean removePermissions(List<Integer> ids) {
        return permissionsDao.remove(ids)>0;
    }

    @Override
    public boolean updatePermissions(Permissions permissions) {
        return permissionsDao.update(permissions)>0;
    }

    @Override
    public int countPermissions() {
        return permissionsDao.count();
    }
}
