package com.baidu.asksys.service.impl;

import com.baidu.asksys.bean.Comment;
import com.baidu.asksys.bean.Tags;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.dao.CommentDao;
import com.baidu.asksys.service.CommentService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;

    @Override
    public List<Comment> listComment(String page, String rows, String cmcontent) {
        int p=Integer.parseInt(page);
        int r=Integer.parseInt(rows);
        return commentDao.list((p-1)*r,r,cmcontent);
    }

    @Override
    public int countComment(String cmcontent) {
        return 0;
    }

    @Override
    public boolean saveComment(Comment comment) {
        return commentDao.save(comment)>0;
    }

    @Override
    public boolean removeComment(List<Integer> ids) {
        User users=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        int uid=users.getUid();
        return commentDao.remove(ids,uid)>0;
    }

    @Override
    public int updateComment(Comment comment) {
        return commentDao.update(comment);
    }

    @Override
    public List<Comment> listComment() {
        return null;
    }
}
