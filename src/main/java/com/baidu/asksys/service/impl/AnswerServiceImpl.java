package com.baidu.asksys.service.impl;

import com.baidu.asksys.bean.Answer;
import com.baidu.asksys.bean.Question;
import com.baidu.asksys.bean.User;
import com.baidu.asksys.dao.AnswerDao;
import com.baidu.asksys.service.AnswerService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerDao answerDao;

    @Override
    public boolean saveAnswer(Answer answer) {
        User user=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        answer.setUid(user.getUid());
        return answerDao.save(answer)>0;
    }

    @Override
    public int count(String title) {
        return answerDao.count(title);
    }

    @Override
    public List<Answer> listQuestion(String page, String rows, String title) {
        int ps = Integer.parseInt(page);
        int rs = Integer.parseInt(rows);
        return answerDao.list((ps - 1) * rs, rs, title);
    }

    @Override
    public boolean update(Answer answer) {
        User user=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        answer.setUid(user.getUid());
        return answerDao.update(answer)>0;
    }

    @Override
    public boolean remove(List<Integer> ids) {
        //取出session中的内容
        User user=(User) SecurityUtils.getSubject().getSession().getAttribute("user");
        int uid=user.getUid();
        return answerDao.remove(ids,uid) > 0;
    }

    @Override
    public boolean likes(int aid) {
        return answerDao.likes(aid)>0;
    }

}
