package com.baidu.asksys.service.impl;

import com.baidu.asksys.bean.User;
import com.baidu.asksys.dao.UserDao;
import com.baidu.asksys.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User login(String uname) {
        return userDao.login(uname);
    }

    @Override
    public boolean save(String uname, String password) {
        return userDao.save(uname, password) > 0;
    }

    @Override
    public List<User> listUser(int page, int rows, String uname) {
        return userDao.list((page-1)*rows, rows, uname);
    }

    @Override
    public int count() {
        return userDao.count();
    }

    @Override
    public boolean remove(List<Integer> ids) {
        User users = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        return userDao.remove(ids, users.getUid()) > 0;
    }

    @Override
    public int update(User user) {
        return userDao.update(user);
    }
}
