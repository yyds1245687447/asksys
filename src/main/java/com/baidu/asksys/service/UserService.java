package com.baidu.asksys.service;

import com.baidu.asksys.bean.User;

import java.util.List;

public interface UserService {

    //    使用Shiro给Service只需传递用户名，返回类型是User对象
    User login(String uname);

    boolean save(String uname, String password);

    List<User> listUser(int page, int rows, String uname);

    int count();

    boolean remove(List<Integer> ids);

    int update(User user);
}
